﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesClassLibrary
{
    public class UserCards
    {
        public int IdUser { get; set; }
        public List<Card> Cards { get; set; }
    }
}
