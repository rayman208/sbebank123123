﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EntitiesClassLibrary
{
    public class Card
    {
        public int Id { get; set; }

        public int Number { get; set; }

        public int Balance { get; set; }
    }
}
