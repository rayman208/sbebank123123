﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbManagerClassLibrary.Tools;
using EntitiesClassLibrary;
using MySql.Data.MySqlClient;

namespace DbManagerClassLibrary.Tables
{
    public class TableUserCards
    {
        public UserCards GetUserCardsByIdUser(int idUser)
        {
            UserCards userCards = new UserCards()
            {
                IdUser = idUser,
                Cards = new List<Card>()
            };

            using (MySqlConnection connection = DbConnector.GetConnection())
            {
                connection.Open();

                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = $@"SELECT * FROM `users_cards` AS uc
                    JOIN `cards` AS c ON uc.id_card = c.id
                    WHERE uc.`id_user`= {idUser}";

                    MySqlDataReader reader = command.ExecuteReader();

                    while (reader.Read() == true)
                    {
                        userCards.Cards.Add(new Card()
                        {
                            Id = reader.GetInt32("id"),
                            Number = reader.GetInt32("number"),
                            Balance = reader.GetInt32("balance"),

                        });
                    }

                    reader.Close();
                }

                connection.Close();
            }

            return userCards;
        }

        public void AddUserCard(int idUser, int idCard)
        {
            using (MySqlConnection connection = DbConnector.GetConnection())
            {
                connection.Open();

                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = $"INSERT INTO `users_cards` (`id_user`,`id_card`) VALUES ({idUser},{idCard})";

                    command.ExecuteNonQuery();
                }

                connection.Close();
            }
        }
    }
}
