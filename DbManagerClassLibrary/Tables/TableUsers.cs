﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbManagerClassLibrary.Tools;
using EntitiesClassLibrary;
using MySql.Data.MySqlClient;

namespace DbManagerClassLibrary.Tables
{
    public class TableUsers
    {
        public User GetUserByLoginAndPassword(string login, string password)
        {
            User user = null;

            using (MySqlConnection connection = DbConnector.GetConnection())
            {
                connection.Open();

                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = $"SELECT * FROM `users` WHERE `login`='{login}' AND `password`='{password}'";

                    MySqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows == true)
                    {
                        reader.Read();

                        user = new User()
                        {
                            Id = reader.GetInt32("id"),
                            Name = reader.GetString("name")
                        };
                    }

                    reader.Close();
                }

                connection.Close();
            }

            return user;
        }
    }
}
