﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DbManagerClassLibrary.Tools;
using EntitiesClassLibrary;
using MySql.Data.MySqlClient;

namespace DbManagerClassLibrary.Tables
{
    public class TableCards
    {
        public Card GetCardByNumber(int number)
        {
            Card card = null;

            using (MySqlConnection connection = DbConnector.GetConnection())
            {
                connection.Open();

                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = $"SELECT * FROM `cards` WHERE `number`={number}";

                    MySqlDataReader reader = command.ExecuteReader();

                    if (reader.HasRows == true)
                    {
                        reader.Read();

                        card = new Card()
                        {
                            Id = reader.GetInt32("id"),
                            Number = reader.GetInt32("number"),
                            Balance = reader.GetInt32("balance"),
                        };
                    }

                    reader.Close();
                }

                connection.Close();
            }

            return card;
        }

        public void AddNewCard(Card card)
        {
            using (MySqlConnection connection = DbConnector.GetConnection())
            {
                connection.Open();

                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText =
                        $"INSERT INTO `cards`(`number`,`balance`) VALUES ({card.Number},{card.Balance})";

                    command.ExecuteNonQuery();
                }

                connection.Close();
            }
        }

        public int GetLastInsertCardId()
        {
            int lastId = 0;
            using (MySqlConnection connection = DbConnector.GetConnection())
            {
                connection.Open();

                using (MySqlCommand command = connection.CreateCommand())
                {
                    command.CommandText ="SELECT last_insert_id()";

                    lastId = Convert.ToInt32(command.ExecuteScalar());
                }

                connection.Close();
            }

            return lastId;
        }
    }
}
